<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Template extends MY_Controller{

    public function index(){
        $this->load->view('header');
        $this->load->view('ver');
        $this->load->view('dtBasicExample');
        $this->load->view('footer');
    }

}